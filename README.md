###### `@Author Saul DSCF <sferreira@liveareacx.com>`

# Docker Generic Magento2 Portable Configurations`(for local-dev environment)`

#### `Tested on:` Generic Magento 2.2+

###### Portability configuration Wiki Table of contents

- [Prerequisites](https://bitbucket.org/sferreira-liveareacx/docker-generic-magento2-portable-configurations/wiki/Home#markdown-header-prerequisites)

- [Setup](https://bitbucket.org/sferreira-liveareacx/docker-generic-magento2-portable-configurations/wiki/Home#markdown-header-setup)

- [Custom CLI Commands](https://bitbucket.org/sferreira-liveareacx/docker-generic-magento2-portable-configurations/wiki/Home#markdown-header-custom-cli-commands)

- [Misc Info](https://bitbucket.org/sferreira-liveareacx/docker-generic-magento2-portable-configurations/wiki/Home#markdown-header-misc-info)

######Thank you
